import React, { ReactNode, createContext, useContext, useState } from 'react';

type Props = {
  children?: ReactNode;
};

type IAuthUser = {
  userId: string;
};

type IAuthContext = {
  authUser: IAuthUser;
  setAuthUser: (authUser: IAuthUser) => void;
};

const initialState = {
  authUser: { userId: '' },
  setAuthUser: (authUser: IAuthUser) => {},
};

const AuthContext = createContext<IAuthContext>(initialState);

const useAuthContext = () => {
  return useContext(AuthContext);
};

const AuthContextProvider = ({ children }: Props) => {
  const [authUser, setAuthUser] = useState(
    JSON.parse(localStorage.getItem('@user') ?? '{}')
  );

  return (
    <AuthContext.Provider value={{ authUser, setAuthUser }}>
      {children}
    </AuthContext.Provider>
  );
};

export { AuthContext, AuthContextProvider, useAuthContext };
