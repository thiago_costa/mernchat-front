import { Home } from './pages/home/Home';
import { Login } from './pages/login/Login';
import { Signup } from './pages/signup/Signup';
import { Routes, Route } from 'react-router-dom';
import Toast from 'react-hot-toast';

function App() {
  return (
    <div className="p-4-screen flex items-center justify-center">
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<Signup />} />
      </Routes>
      <Toast />
    </div>
  );
}

export default App;
